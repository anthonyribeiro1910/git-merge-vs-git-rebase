const bank = 'Banco do Nordeste';

console.log(bank);

const studentsNumber = 18;

console.log(`O número de alunos nesta classe é ${studentsNumber}`);

console.log(`Este curso está sendo realizado na data de ${new Date()}`);

const sum = (a, b) => a + b;

const name = 'João';

const country = 'Brasil';

console.log(country);

const countries = ['Brasil', 'Argentina', 'Inglaterra'];

console.log(countries.join(', '));